import React from 'react'

export default props => (
    <footer className='main-footer'> 
        <strong> 
            Copyright &copy; 2018
            <a href='http://' target='_blank'> TMSystem</a>.
        </strong>
    </footer>
)