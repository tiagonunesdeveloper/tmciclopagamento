import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as toastrReducer } from 'react-redux-toastr'

import DashboardReducer from '../componentes/Dashboard/store/dashboardReducer'
import TabReducer from '../common/tab/store/tabReducer'
import CicloPagamentosReducer from '../componentes/CicloPagamento/store/cicloPagamentoReducer'

const rootReducer = combineReducers({
    dashboard: DashboardReducer,
    tab: TabReducer,
    cicloPagamento: CicloPagamentosReducer,
    form: formReducer,
    toastr: toastrReducer,
})

export default rootReducer