import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getList, showUpdate, showDelete } from './store/cicloPagamentoActions'

class CicloPagamentoLista extends Component {

    componentWillMount() {
        this.props.getList()
    }

    renderRows() {
        const lista = this.props.lista || []
        return lista.map(cp => (
            <tr key={cp.id}>
                <td>{cp.nome}</td>
                <td>{cp.mes}</td>
                <td>{cp.ano}</td>
                <td>
                    <button className='btn btn-warning' onClick={() => this.props.showUpdate(cp)}>
                        <i className='fa fa-pencil'></i>
                    </button>
                    <button className='btn btn-danger' onClick={() => this.props.showDelete(cp)}>
                        <i className='fa fa-trash-o'></i>
                    </button>
                </td>
            </tr>
        ))
    }

    render() {

        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Mês</th>
                            <th>Ano</th>
                            <th className='table-actions'>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
            </div>
        )
    }

}

const mapStateToProps = state => ({ lista: state.cicloPagamento.lista })
const mapDispatchToProps = dispatch => bindActionCreators({ getList, showUpdate, showDelete }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(CicloPagamentoLista)