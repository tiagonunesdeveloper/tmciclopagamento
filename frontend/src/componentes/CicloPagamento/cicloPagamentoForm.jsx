import React, { Component } from 'react'
import { reduxForm, Field, formValueSelector } from 'redux-form'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import LabelAndInput from '../../common/form/labelAndInput'

import ItemList from './itemList'
import Summary from './summary'

import { init } from './store/cicloPagamentoActions'

class CicloPagamentoForm extends Component {

  calcularSumario() {
    const soma = (t, v) => (t + v)
    return {
      somaDosCreditos: this.props.creditos.map(c => +c.valor || 0).reduce(soma),
      somaDosDebitos: this.props.debitos.map(d => +d.valor || 0).reduce(soma),
      total: this.props.creditos.map(c => +c.valor || 0).reduce(soma) - this.props.debitos.map(d => +d.valor || 0).reduce(soma)
    }
  }

  valorReal(valor) {
    return valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
  }

  render() {
    const { handleSubmit, readOnly, creditos, debitos } = this.props
    const { somaDosCreditos, somaDosDebitos, total } = this.calcularSumario()
    return (
      <form role='form' onSubmit={handleSubmit}>
        <div className='box-body'>
          <Field
            name='nome'
            readOnly={readOnly}
            component={LabelAndInput}
            label='Nome'
            cols='12 4'
            placeholder='Infome o nome'
            type='text' />
          <Field
            name='mes'
            readOnly={readOnly}
            component={LabelAndInput}
            label='Mês'
            cols='12 4'
            placeholder='Informe o mês'
            type='number' />
          <Field
            name='ano'
            readOnly={readOnly}
            component={LabelAndInput}
            label='Ano'
            cols='12 4'
            placeholder='Informe o ano'
            type='number' />

          <Summary
            credito={this.valorReal(somaDosCreditos)}
            debito={this.valorReal(somaDosDebitos)}
            total={this.valorReal(total)} />

          <ItemList
            cols='12 6'
            readOnly={readOnly}
            form='cicloPagamentoForm'
            field='creditos'
            tablesHeaders={['Nome', 'Valor', 'Ação']}
            list={creditos}
            legend='Créditos' />
          <ItemList
            cols='12 6'
            readOnly={readOnly}
            form='cicloPagamentoForm'
            field='debitos'
            tablesHeaders={['Nome', 'Valor', 'Status', 'Ação']}
            list={debitos}
            legend='Débitos' />
        </div>
        <div className='box-footer'>
          <button type='submit' className={`btn btn-${this.props.submitClass}`} >{this.props.submitLabel}</button>
          <button type='button' className='btn btn-default' onClick={this.props.init}>Cancelar</button>
        </div>
      </form>
    )
  }
}

CicloPagamentoForm = reduxForm({ form: 'cicloPagamentoForm', destroyOnUnmount: false })(CicloPagamentoForm)
const selector = formValueSelector('cicloPagamentoForm')
const mapStateToProps = state => ({
  creditos: selector(state, 'creditos'),
  debitos: selector(state, 'debitos')
})
const mapDispatchToPros = dispatch => bindActionCreators({ init }, dispatch)
export default connect(mapStateToProps, mapDispatchToPros)(CicloPagamentoForm)