import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import ContentHeader from '../../common/template/contentHeader'
import Content from '../../common/template/content'
import Row from '../../common/layout/row'
import Tabs from '../../common/tab/tabs'
import TabsHeader from '../../common/tab/tabsHeader'
import TabsContent from '../../common/tab/tabsContent'
import TabHeader from '../../common/tab/tabHeader'
import TabContent from '../../common/tab/tabContent'

import Lista from './cicloPagamentoLista'
import Form from './cicloPagamentoForm'

import { create, update, remove, init } from './store/cicloPagamentoActions'

class CicloPagamento extends Component {

	componentWillMount() {
		this.props.init()
	}

	render() {
		return (
			<div>
				<ContentHeader title='Ciclos Pagamentos' small='Cadastro' />
				<Content>
					<Tabs>
						<TabsHeader>
							<TabHeader target='tabList' icon='bars' label='Listar' />
							<TabHeader target='tabCreate' icon='plus' label='Incluir' />
							<TabHeader target='tabUpdate' icon='pencil' label='Alterar' />
							<TabHeader target='tabDelete' icon='trash-o' label='Excluir' />
						</TabsHeader>
						<TabsContent>
							<TabContent id='tabList'>

								<Lista />

							</TabContent>
							<TabContent id='tabCreate'>

								<Form onSubmit={this.props.create} submitClass='primary' submitLabel='Incluir' />

							</TabContent>
							<TabContent id='tabUpdate'>

								<Form onSubmit={this.props.update} submitClass='warning' submitLabel='Alterar' />

							</TabContent>
							<TabContent id='tabDelete'>

								<Form onSubmit={this.props.remove} submitClass='danger' readOnly={true} submitLabel='Excluir' />

							</TabContent>
						</TabsContent>
					</Tabs>
				</Content>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => bindActionCreators(
	{ create, update, remove, init },
	dispatch)
export default connect(null, mapDispatchToProps)(CicloPagamento)