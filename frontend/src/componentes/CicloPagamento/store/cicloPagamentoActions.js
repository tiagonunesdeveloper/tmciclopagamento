import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import { reset as resetForm, initialize } from 'redux-form'

import { showTabs, selectTab } from '../../../common/tab/store/tabActions'

import { BASE_URL } from '../../../common/utils/BASE_URL'

const INITIAL_VALUES = {
  creditos: [{nome: '', valor: ''}],
  debitos: [{nome: '', valor: ''}],
}

const getList = () => {
  const request = axios.get(`${BASE_URL}/ciclospagamentos`)
  return {
    type: 'CICLO_PAGAMENTO_FETCHED',
    payload: request
  }
}

const create = (values) => {
  return submit(values, 'post', 'Criação')
}

const update = (values) => {
  return submit(values, 'put', 'Alteração')
}

const remove = (values) => {
  return submit(values, 'delete', 'Exclusão')
}

function submit(values, method, title) {
  const id = values.id ? values.id : ''
  return dispatch => {
    axios[method](`${BASE_URL}/ciclospagamentos/${id}`, values)
      .then(resp => {
        toastr.success(title, 'Operação Realizada com Sucesso.')
        dispatch(init())
      })
      .catch(e => {
        toastr.error('Ops!', 'Erro ao Processar a Operação.')
      })
  }
}

const showUpdate = (cicloPagamento) => {
  return showMethodTabs('tabUpdate', cicloPagamento)
}

const showDelete = (cicloPagamento) => {
  return showMethodTabs('tabDelete', cicloPagamento)
}

function showMethodTabs(tab, cp) {
  return [
    showTabs(tab),
    selectTab(tab),
    initialize('cicloPagamentoForm', cp),
  ]
}

const init = () => {
  return [
    getList(),
    selectTab('tabList'),
    showTabs('tabList', 'tabCreate'),
    initialize('cicloPagamentoForm', INITIAL_VALUES),
  ]
}

export { getList, create, update, remove, showUpdate, showDelete, init }