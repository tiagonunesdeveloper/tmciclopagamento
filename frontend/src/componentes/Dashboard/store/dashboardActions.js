import axios from 'axios'
import { BASE_URL } from '../../../common/utils/BASE_URL'

const getSummary = () => {
    const request = axios.get(`${BASE_URL}/summary`)
    return {
        type: 'BILLING_SUMMARY_FETCHED',
        payload: request
    }
}

export { getSummary }