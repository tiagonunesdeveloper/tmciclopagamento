import React from 'react'
import { Router, Route, IndexRoute, Redirect, hashHistory } from 'react-router'

import Dashboard from '../componentes/Dashboard/dashboard'
import CicloPagamento from '../componentes/CicloPagamento/cicloPagamento'

export default props => (
    <Router history={hashHistory}>
        <Route path='/' component={Dashboard} />
        <Route path='ciclospagamentos' component={CicloPagamento} />
        <Redirect from='*' to='/' />
    </Router>
)